using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Submodules.MaverickUtils
{
    public static class ExtensionMethods
    {
        #region Vector3

        public static Vector2 XY(this Vector3 v)
        {
            return new Vector2(v.x, v.y);
        }

        public static Vector2 XZ(this Vector3 v)
        {
            return new Vector2(v.x, v.z);
        }

        public static Vector3 ZeroZ(this Vector3 v)
        {
            return new Vector3(v.x, v.y, 0f);
        }   public static Vector3 ZeroY(this Vector3 v)
        {
            return new Vector3(v.x, 0, v.z);
        }

        public static Vector2 GetRandomDirectionNormalizedVector(float minAngle, float maxAngle)
        {
            Vector2 result;
            float angle = Random.Range(minAngle, maxAngle) / Mathf.PI;
            result.x = Mathf.Cos(angle);
            result.y = Mathf.Sin(angle);
            return result;
        }

        public static Vector3 WithX(this Vector3 v, float x)
        {
            return new Vector3(x, v.y, v.z);
        }

        public static Vector3 WithY(this Vector3 v, float y)
        {
            return new Vector3(v.x, y, v.z);
        }

        public static Vector3 WithZ(this Vector3 v, float z)
        {
            return new Vector3(v.x, v.y, z);
        }

        #endregion

        #region Vector2

        public static Vector3 XYZ(this Vector2 v)
        {
            return new Vector3(v.x, v.y, 0f);
        }

        public static Vector3 WithZ(this Vector2 v, float z)
        {
            return new Vector3(v.x, v.y, z);
        }

        public static Vector2 WithX(this Vector2 v, float x)
        {
            return new Vector2(x, v.y);
        }

        public static Vector2 WithY(this Vector2 v, float y)
        {
            return new Vector2(v.x, y);
        }

        #endregion

        #region Transform

        public static void ResetTransformation(this Transform trans)
        {
            trans.position = Vector3.zero;
            trans.localRotation = Quaternion.identity;
            trans.localScale = new Vector3(1, 1, 1);
        }

        #endregion

        #region List

        public static void Shuffle<T>(this IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = Random.Range(0, n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        #endregion

        #region Action

        public static void Call<T>(this Action<T> action, T param)
        {
            if (action != null) action(param);
        }

        public static void Call(this Action action)
        {
            if (action != null) action();
        }

        #endregion
    }
}