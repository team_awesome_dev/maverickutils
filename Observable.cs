using System;

namespace Submodules.MaverickUtils
{
    public class Observable<T>
    {
        public event Action<T> OnUpdatedEvent;

        private T _value;

        public Observable(T defaultValue = default(T))
        {
            _value = defaultValue;
        }

        public virtual T Value
        {
            get { return _value; }
            set
            {
                if (_value != null && !_value.Equals(value))
                {
                    _value = value;
                    OnUpdatedEvent.Call(_value);
                }
                else if (_value == null && value != null)
                {
                    _value = value;
                    if (OnUpdatedEvent != null) OnUpdatedEvent(_value);
                }
            }
        }
    }
}