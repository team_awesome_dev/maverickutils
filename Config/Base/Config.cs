using System;
using UnityEngine;

namespace Submodules.MaverickUtils.Config.Base
{
    public interface IConfig
    {
    }

    [Serializable]
    public class Config : ScriptableObject, IConfig
    {
    }
}