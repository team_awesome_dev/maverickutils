using System;
using System.Collections;
using System.Collections.Generic;

namespace Submodules.MaverickUtils
{
    public class ObservableList<T> : IList<T>
    {
        public event Action<T> OnAddedEvent;
        public event Action<T> OnRemovedEvent;
        public event Action<IList<T>> OnUpdatedEvent;

        private List<T> _value;

        public ObservableList(List<T> list = null)
        {
            if (list != null)
                _value = list;
            else
                _value = new List<T>();
        }

        public void Add(T item)
        {
            _value.Add(item);
            OnUpdatedEvent.Call(_value);
            OnAddedEvent.Call(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            _value.CopyTo(array, arrayIndex);
        }

        public bool Remove(T item)
        {
            var result = _value.Remove(item);
            if (result)
            {
                OnUpdatedEvent.Call(_value);
                OnRemovedEvent.Call(item);
            }

            return result;
        }

        public int Count
        {
            get { return _value.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }


        public void Clear()
        {
            foreach (var item in _value)
            {
                OnRemovedEvent.Call(item);
            }

            _value.Clear();
        }

        public void AddRange(List<T> range)
        {
            _value.AddRange(range);
            OnUpdatedEvent.Call(_value);
        }

        public bool Contains(T item)
        {
            return _value.Contains(item);
        }


        public virtual List<T> Value
        {
            get { return _value; }
            set
            {
                if (_value != null && !_value.Equals(value))
                {
                    _value = value;
                    OnUpdatedEvent.Call(_value);
                }
                else if (_value == null)
                {
                    _value = value;
                    OnUpdatedEvent.Call(_value);
                }
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            return _value.GetEnumerator();
        }


        public int IndexOf(T item)
        {
            return _value.IndexOf(item);
        }

        public void Insert(int index, T item)
        {
            _value.Insert(index, item);
            OnUpdatedEvent.Call(_value);
        }

        public void RemoveAt(int index)
        {
            var result = _value[index];
            _value.RemoveAt(index);
            OnUpdatedEvent.Call(_value);
            OnRemovedEvent.Call(result);
        }

        public T this[int index]
        {
            get { return _value[index]; }
            set { _value[index] = value; }
        }


        public T Find(Predicate<T> match)
        {
            return _value.Find(match);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _value.GetEnumerator();
        }
    }
}