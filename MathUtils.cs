﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Submodules.MaverickUtils
{
    public static class MathUtils
    {
        public static float AngleSigned(Vector3 v1, Vector3 v2, Vector3 n)
        {
            return Mathf.Atan2(
                       Vector3.Dot(n, Vector3.Cross(v1, v2)),
                       Vector3.Dot(v1, v2)) * Mathf.Rad2Deg;
        }

        /// <summary>
        ///  Accepts range of integers and produces random number from range without certain number of repetition
        /// </summary>
        public class RandomNotSameIntRange
        {
            public readonly int Min;
            public readonly int Max;
            public readonly int NoRepeat;
            private readonly LinkedList<int> _lastUsed = new LinkedList<int>();
            private readonly List<int> _unused = new List<int>();

            /// <summary>
            /// Accepts range of integers (0-x) and produces random number from range without depth number of repetition
            /// </summary>
            /// <param name="min"> Minimal integer of the range (inclusive, must be less then max) </param>
            /// <param name="max"> Maximum integer of the range (inclusive,mast be greater then min and noRepeat)</param>
            /// <param name="noRepeat"> No repetition amount (must be in range: is less then  max-min </param>
            public RandomNotSameIntRange(int min, int max, int noRepeat = 1)
            {
                Assert.IsTrue(min >= 0);
                Assert.IsTrue(max > 1);
                Assert.IsTrue(noRepeat < max);
                Assert.IsTrue(noRepeat >= 0);
                Min = min;
                Max = max;
                NoRepeat = Mathf.Clamp(noRepeat, 0, Max - 1);
                for (int i = Min; i < Max; i++)
                {
                    _unused.Add(i);
                }
            }

            public int Get()
            {
                var generatedIndex = Random.Range(0, _unused.Count);
//               
                var result = _unused[generatedIndex];


                _unused.Remove(result);
                _lastUsed.AddLast(result);

                if (_lastUsed.First != null && _lastUsed.Count > NoRepeat)
                {
                    var returnValue = _lastUsed.First.Value;
                    _unused.Add(returnValue);
                    _lastUsed.RemoveFirst();
                }


                return result;
            }
        }

        public class WeightedRandom<T>
        {
            private readonly Dictionary<T, float> _weights;

            public WeightedRandom(Dictionary<T, float> weights)
            {
                _weights = weights;
            }

            public T GetWeightedRandom()
            {
                var sortedWeights = Sort(_weights);

                float sum = 0f;
                foreach (var weight in _weights)
                {
                    sum += weight.Value;
                }

                float prob = Random.Range(0f, sum);

                T result = sortedWeights[sortedWeights.Count - 1].Key; // the most likely

                foreach (var pair in sortedWeights)
                {
                    if (prob < pair.Value)
                    {
                        result = pair.Key;
                        break;
                    }

                    prob -= pair.Value;
                }

                return result;
            }


            private List<KeyValuePair<T, float>> Sort(Dictionary<T, float> weights)
            {
                var list = new List<KeyValuePair<T, float>>(weights);


                list.Sort(
                    (firstPair, nextPair) => firstPair.Value.CompareTo(nextPair.Value)
                );

                return list;
            }
        }
    }
}