using System;
using System.Collections.Generic;
using UnityEngine;

namespace Submodules.MaverickUtils
{

    public class FloatModifier
    {

        public float Value;
        public float TimeToStop;

        public FloatModifier(float value, float duration)
        {
            Value = value;
            TimeToStop = Time.time + duration;
            Duration =  duration;
        }

        public float Duration;

        public virtual float Sample(float input)
        {
            return input;
        }


    }
    public class FloatMultiplier : FloatModifier
    {
        public FloatMultiplier(float multiplier,float duration) : base(multiplier,duration)
        {
           
        }

        public override float Sample(float input)
        {
            
            return input * Value;
        }
    }
    
    public class FloatCurvedMultiplier : FloatModifier
    {
        private AnimationCurve Curve;
        private float t;
        private float mod;

        public FloatCurvedMultiplier(float multiplier,float duration, AnimationCurve curve) : base(multiplier,duration)
        {
            Curve = curve;
        }

        public override float Sample(float input)
        {
            t = 1f - (TimeToStop -Time.time )/Duration;
            mod = input* Value-input;
            return input + mod*Curve.Evaluate(t);
        }
    }
    
    public class FloatIncrement : FloatModifier
    {
        public FloatIncrement(float multiplier,float duration) : base(multiplier,duration)
        {   
        }

        public override float Sample(float input)
        {
            return  input + Value;
        }
    }
    
    public class ObservableModifiedFloat
    {
        public event Action<float> OnUpdatedEvent;
        public FloatModifier modifier;
        private float _value;

        public void IncrementModifier(float value, float duration)
        {
            modifier = new FloatIncrement(value,duration);
        }
        
        public void MultiplyModifier(float value, float duration)
        {
            modifier = new FloatMultiplier(value,duration);
        }
        public ObservableModifiedFloat(float defaultValue = float.NaN)
        {
            modifier = new FloatModifier(0f,0f);
            _value = defaultValue;
        }

        public virtual float Value
        {
            get
            {
                if (Time.time<modifier.TimeToStop )
                {
                    return modifier.Sample(_value);
                }

                return _value;
            }
         
        }

        public virtual float DefaultValue
        {
            get {
                 return _value;
            }
            set
            {
                if (!float.IsNaN(_value) && !_value.Equals(value))
                {
                    _value = value;
                    if (OnUpdatedEvent != null) OnUpdatedEvent.Call(_value);
                }
                else if (float.IsNaN(_value) && !float.IsNaN(_value))
                {
                    _value = value;
                    if (OnUpdatedEvent != null) OnUpdatedEvent(_value);
                }
            }
        }

        public void CurvedMultiplyModifier(float multiplier, float duration,AnimationCurve curve)
        {
            modifier = new FloatCurvedMultiplier(multiplier,duration,curve);
        }
    }
}